# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [3.0.0 - Unreleased]

### Changed
- [SCHEDULER-69] Made project compatible with Java 9, 10 and 11
- [SCHEDULER-69] Now depends on Platform 5 being provided
- [SCHEDULER-69] API change - All methods that previously threw `com.atlassian.util.concurrent.NullArgumentException`
  when a method argument was null (and null is not permitted) now throw `java.lang.NullPointerException`

[SCHEDULER-69]: https://jira.atlassian.com/browse/SCHEDULER-69
