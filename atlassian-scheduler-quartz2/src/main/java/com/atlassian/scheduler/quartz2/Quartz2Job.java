package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.core.AbstractSchedulerService;
import com.atlassian.scheduler.core.JobLauncher;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.spi.TriggerFiredBundle;

/**
 * Quartz 2.x {@code Job} that delegates to {@link JobLauncher}.
 *
 * @since v1.0
 */
public class Quartz2Job implements Job {
    private final JobLauncher jobLauncher;

    Quartz2Job(final AbstractSchedulerService schedulerService, final RunMode schedulerRunMode,
               final TriggerFiredBundle bundle) {
        final JobId jobId = JobId.of(bundle.getTrigger().getKey().getName());
        this.jobLauncher = new JobLauncher(schedulerService, schedulerRunMode, bundle.getFireTime(), jobId);
    }

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        jobLauncher.launch();
    }

    @Override
    public String toString() {
        return "Quartz2Job[jobLauncher=" + jobLauncher + ']';
    }
}

