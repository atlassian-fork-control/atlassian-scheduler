package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.core.tests.CronFactory;
import org.joda.time.DateTimeZone;
import org.quartz.CronExpression;

import javax.annotation.Nullable;
import java.text.ParseException;
import java.util.Date;

/**
 * @since v1.5
 */
public class Quartz2CronFactory implements CronFactory {
    @Override
    public void parseAndDiscard(String cronExpression) {
        parseInternal(cronExpression);
    }

    @Override
    public CronExpressionAdapter parse(final String cronExpression) {
        return parse(cronExpression, DateTimeZone.getDefault());
    }

    @Override
    public CronExpressionAdapter parse(final String cronExpression, final DateTimeZone zone) {
        final CronExpression parsed = parseInternal(cronExpression);
        parsed.setTimeZone(zone.toTimeZone());

        return new CronExpressionAdapter() {
            @Override
            public boolean isSatisfiedBy(Date date) {
                return parsed.isSatisfiedBy(date);
            }

            @Nullable
            @Override
            public Date nextRunTime(Date prevRunTime) {
                return parsed.getNextValidTimeAfter(prevRunTime);
            }

            @Override
            public String toString() {
                return cronExpression;
            }
        };
    }

    private static CronExpression parseInternal(final String cronExpression) {
        try {
            return new CronExpression(cronExpression);
        } catch (ParseException pe) {
            final AssertionError err = new AssertionError("Unable to parse cron expression '" + cronExpression + '\'');
            err.initCause(pe);
            throw err;
        }
    }
}
