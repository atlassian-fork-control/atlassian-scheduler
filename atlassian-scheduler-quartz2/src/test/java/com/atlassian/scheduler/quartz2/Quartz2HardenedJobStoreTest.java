package com.atlassian.scheduler.quartz2;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.JobPersistenceException;
import org.quartz.TriggerKey;
import org.quartz.impl.jdbcjobstore.DriverDelegate;
import org.quartz.impl.jdbcjobstore.NoSuchDelegateException;
import org.quartz.spi.OperableTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerKey.triggerKey;
import static org.quartz.impl.jdbcjobstore.Constants.STATE_MISFIRED;

public class Quartz2HardenedJobStoreTest {
    private static final Logger LOG = LoggerFactory.getLogger(Quartz2HardenedJobStoreTest.class);

    private static final JobKey JOB1_KEY = jobKey("job1name", "job1group");
    private static final JobKey JOB2_KEY = jobKey("job2name", "job2group");
    private static final TriggerKey TRIGGER1_KEY = triggerKey("trigger1name", "trigger1group");
    private static final TriggerKey TRIGGER2_KEY = triggerKey("trigger2name", "trigger2group");

    private final AtomicInteger callCount = new AtomicInteger();
    private final AtomicInteger throwCount = new AtomicInteger();
    private final AtomicInteger escapeCount = new AtomicInteger();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule().silent();

    @Mock
    private Connection conn;
    @Mock
    private DriverDelegate delegate;
    @Mock
    private OperableTrigger trigger1;
    @Mock
    private OperableTrigger trigger2;
    @Mock
    private JobDetail job1;
    @Mock
    private JobDetail job2;

    @Before
    public void setUp() throws Exception {
        when(trigger1.getJobKey()).thenReturn(JOB1_KEY);
        when(trigger2.getJobKey()).thenReturn(JOB2_KEY);
        when(trigger1.getKey()).thenReturn(TRIGGER1_KEY);
        when(trigger2.getKey()).thenReturn(TRIGGER2_KEY);

        when(delegate.selectTriggersForRecoveringJobs(conn)).thenReturn(asList(trigger1, trigger2));
        when(delegate.selectJobDetail(eq(conn), eq(JOB1_KEY), any())).thenAnswer(new Answer<JobDetail>() {
            @Override
            public JobDetail answer(InvocationOnMock invocation) throws Throwable {
                throwCount.incrementAndGet();
                throw new ClassNotFoundException("He's DEAD, Jim!");
            }
        });
        when(delegate.selectJobDetail(eq(conn), eq(JOB2_KEY), any()))
                .thenReturn(job2);
    }

    @Test
    public void testStoreTriggerThrowsExceptionOutsideOfRecovery() throws JobPersistenceException {
        final Fixture fixture = new Fixture();
        try {
            fixture.storeTrigger(conn, trigger1, null, true, STATE_MISFIRED, true, true);
            fail("Should have thrown JobPersistenceException");
        } catch (JobPersistenceException jpe) {
            assertThat(jpe.getMessage(), containsString("He's DEAD, Jim!"));
            assertEquals("callCount", 1, callCount.get());
            assertEquals("throwCount", 1, throwCount.get());
            assertEquals("escapeCount", 1, escapeCount.get());
        }
    }

    @Test
    public void testStoreTriggerDoesNotThrowExceptionFromRecovery() throws Exception {
        final Fixture fixture = new Fixture();
        fixture.recoverJobs();
        assertEquals("callCount", 2, callCount.get());
        assertEquals("throwCount", 1, throwCount.get());
        assertEquals("escapeCount", 0, escapeCount.get());
    }

    class Fixture extends Quartz2HardenedJobStore {

        @Override
        protected DriverDelegate getDelegate() throws NoSuchDelegateException {
            return delegate;
        }

        @Override
        protected Object executeInLock(String lockName, TransactionCallback txCallback) throws JobPersistenceException {
            return txCallback.execute(conn);
        }

        @Override
        protected void executeInNonManagedTXLock(
                final String lockName,
                final VoidTransactionCallback txCallback) throws JobPersistenceException {
            txCallback.execute(conn);
        }

        @Override
        protected boolean jobExists(Connection conn, JobKey jobKey) throws JobPersistenceException {
            return true;
        }

        @Override
        protected Logger getLog() {
            return LOG;
        }

        @Override
        protected void storeTrigger(Connection conn, OperableTrigger newTrigger, @Nullable JobDetail job,
                                    boolean replaceExisting, String state, boolean forceState, boolean recovering)
                throws JobPersistenceException {
            callCount.incrementAndGet();
            boolean ok = false;
            try {
                super.storeTrigger(conn, newTrigger, job, replaceExisting, state, forceState, recovering);
                ok = true;
            } finally {
                if (!ok) {
                    escapeCount.incrementAndGet();
                }
            }
        }
    }
}
