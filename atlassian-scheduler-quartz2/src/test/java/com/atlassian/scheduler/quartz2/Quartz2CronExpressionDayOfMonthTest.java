package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.core.tests.CronExpressionDayOfMonthTest;
import org.junit.Test;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.unsatisfiedBy;

/**
 * @since v1.5
 */
public class Quartz2CronExpressionDayOfMonthTest extends CronExpressionDayOfMonthTest {
    public Quartz2CronExpressionDayOfMonthTest() {
        super(new Quartz2CronFactory());
    }

    @Override
    @Test
    public void testNearestWeekdayToLastDayOfMonthEdgeCase() {
        assertQuartzBug("Edge case - 2014/28/02 and 29W", "0 0 0 29W * ?", unsatisfiedBy(0, 0, 0, 28, 2, 2014));
        assertQuartzBug("Edge case - 2010/04/30 and 31W", "0 0 0 31W * ?", unsatisfiedBy(0, 0, 0, 30, 4, 2010));
    }
}
