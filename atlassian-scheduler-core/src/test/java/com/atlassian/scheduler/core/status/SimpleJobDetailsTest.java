package com.atlassian.scheduler.core.status;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.status.JobDetails;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.scheduler.config.Schedule.runOnce;
import static com.atlassian.scheduler.core.Constants.JOB_ID;
import static com.atlassian.scheduler.core.Constants.KEY;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @since v1.0
 */
@SuppressWarnings({"ResultOfObjectAllocationIgnored", "CastToConcreteClass", "ConstantConditions"})
public class SimpleJobDetailsTest {
    private static final byte[] RAW_PARAMETERS = new byte[]{1, 2, 3, 4};
    private static final Map<String, Serializable> NO_PARAMETERS = ImmutableMap.of();
    private static final Map<String, Serializable> PARAMETERS = ImmutableMap.<String, Serializable>builder()
            .put("Hello", 42L)
            .put("World", true)
            .build();

    @Test(expected = NullPointerException.class)
    public void testJobIdNull() {
        new SimpleJobDetails(null, KEY, RUN_LOCALLY, runOnce(null), new Date(), RAW_PARAMETERS, PARAMETERS);
    }

    @Test(expected = NullPointerException.class)
    public void testJobRunnerKeyNull() {
        new SimpleJobDetails(JOB_ID, null, RUN_LOCALLY, runOnce(null), new Date(), RAW_PARAMETERS, PARAMETERS);
    }

    @Test(expected = NullPointerException.class)
    public void testRunModeNull() {
        new SimpleJobDetails(JOB_ID, KEY, null, runOnce(null), new Date(), RAW_PARAMETERS, PARAMETERS);
    }

    @Test(expected = NullPointerException.class)
    public void testScheduleNull() {
        new SimpleJobDetails(JOB_ID, KEY, RUN_LOCALLY, null, new Date(), RAW_PARAMETERS, PARAMETERS);
    }

    @Test
    public void testValues1() {
        final Date nextRunTime = new Date();
        final Date expectedNextRunTime = new Date(nextRunTime.getTime());
        final JobDetails jobDetails = new SimpleJobDetails(JOB_ID, KEY, RUN_LOCALLY, runOnce(null), nextRunTime, null, null);

        assertEquals(JOB_ID, jobDetails.getJobId());
        assertEquals(KEY, jobDetails.getJobRunnerKey());
        assertEquals(RUN_LOCALLY, jobDetails.getRunMode());
        assertEquals(runOnce(null), jobDetails.getSchedule());
        assertEquals(NO_PARAMETERS, jobDetails.getParameters());
        assertNull(((AbstractJobDetails) jobDetails).getRawParameters());
        assertTrue("Should be runnable", jobDetails.isRunnable());

        nextRunTime.setTime(42L);
        assertEquals(expectedNextRunTime, jobDetails.getNextRunTime());
        jobDetails.getNextRunTime().setTime(42L);
        assertEquals(expectedNextRunTime, jobDetails.getNextRunTime());
    }

    @Test
    public void testValues2() {
        final JobDetails jobDetails = new SimpleJobDetails(JobId.of("x"), JobRunnerKey.of("z"), RUN_ONCE_PER_CLUSTER,
                runOnce(new Date(42L)), null, RAW_PARAMETERS, PARAMETERS);
        assertEquals(JobId.of("x"), jobDetails.getJobId());
        assertEquals(JobRunnerKey.of("z"), jobDetails.getJobRunnerKey());
        assertEquals(RUN_ONCE_PER_CLUSTER, jobDetails.getRunMode());
        assertEquals(runOnce(new Date(42L)), jobDetails.getSchedule());
        assertNull(jobDetails.getNextRunTime());
        assertArrayEquals(RAW_PARAMETERS, ((AbstractJobDetails) jobDetails).getRawParameters());
        assertEquals(PARAMETERS, jobDetails.getParameters());
        assertTrue("Should be runnable", jobDetails.isRunnable());
    }
}
