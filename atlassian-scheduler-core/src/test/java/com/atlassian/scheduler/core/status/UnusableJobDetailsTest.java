package com.atlassian.scheduler.core.status;

import com.atlassian.scheduler.SchedulerRuntimeException;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.core.JobRunnerNotRegisteredException;
import com.atlassian.scheduler.status.JobDetails;
import org.junit.Test;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.scheduler.config.Schedule.runOnce;
import static com.atlassian.scheduler.core.Constants.JOB_ID;
import static com.atlassian.scheduler.core.Constants.KEY;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * @since v1.0
 */
@SuppressWarnings({"ResultOfObjectAllocationIgnored", "CastToConcreteClass", "ConstantConditions"})
public class UnusableJobDetailsTest {
    private static final byte[] RAW_PARAMETERS = new byte[]{1, 2, 3, 4};
    private static final Throwable CAUSE = new Throwable("For test");

    @Test(expected = NullPointerException.class)
    public void testJobIdNull() {
        new UnusableJobDetails(null, KEY, RUN_LOCALLY, runOnce(null), new Date(), RAW_PARAMETERS, CAUSE);
    }

    @Test(expected = NullPointerException.class)
    public void testJobRunnerKeyNull() {
        new UnusableJobDetails(JOB_ID, null, RUN_LOCALLY, runOnce(null), new Date(), RAW_PARAMETERS, CAUSE);
    }

    @Test(expected = NullPointerException.class)
    public void testRunModeNull() {
        new UnusableJobDetails(JOB_ID, KEY, null, runOnce(null), new Date(), RAW_PARAMETERS, CAUSE);
    }

    @Test(expected = NullPointerException.class)
    public void testScheduleNull() {
        new UnusableJobDetails(JOB_ID, KEY, RUN_LOCALLY, null, new Date(), RAW_PARAMETERS, CAUSE);
    }

    @Test
    public void testGetParametersWithNoCauseProvided() {
        final JobDetails jobDetails = new UnusableJobDetails(JOB_ID, KEY, RUN_LOCALLY, runOnce(null), null, null, null);
        assertNull(((AbstractJobDetails) jobDetails).getRawParameters());
        try {
            final Map<String, Serializable> parameters = jobDetails.getParameters();
            fail("Should throw SchedulerRuntimeException, but got: " + parameters);
        } catch (SchedulerRuntimeException sre) {
            final JobRunnerNotRegisteredException cause = (JobRunnerNotRegisteredException) sre.getCause();
            assertEquals(KEY, cause.getJobRunnerKey());
        }
    }

    @Test
    public void testGetParametersWithCauseProvided() {
        final JobDetails jobDetails = new UnusableJobDetails(JOB_ID, KEY, RUN_LOCALLY, runOnce(null), null, RAW_PARAMETERS, CAUSE);
        assertArrayEquals(RAW_PARAMETERS, ((AbstractJobDetails) jobDetails).getRawParameters());
        try {
            final Map<String, Serializable> parameters = jobDetails.getParameters();
            fail("Should throw SchedulerRuntimeException, but got: " + parameters);
        } catch (SchedulerRuntimeException sre) {
            assertEquals(CAUSE, sre.getCause());
        }
    }

    @Test
    public void testValues1() {
        final Date nextRunTime = new Date();
        final Date expectedNextRunTime = new Date(nextRunTime.getTime());

        final JobDetails jobDetails = new UnusableJobDetails(JOB_ID, KEY, RUN_LOCALLY, runOnce(null), nextRunTime, RAW_PARAMETERS, null);
        assertEquals(JOB_ID, jobDetails.getJobId());
        assertEquals(KEY, jobDetails.getJobRunnerKey());
        assertEquals(RUN_LOCALLY, jobDetails.getRunMode());
        assertEquals(runOnce(null), jobDetails.getSchedule());
        assertArrayEquals(RAW_PARAMETERS, ((AbstractJobDetails) jobDetails).getRawParameters());
        assertFalse("Should not be runnable", jobDetails.isRunnable());

        nextRunTime.setTime(42L);
        assertEquals(expectedNextRunTime, jobDetails.getNextRunTime());
        jobDetails.getNextRunTime().setTime(42L);
        assertEquals(expectedNextRunTime, jobDetails.getNextRunTime());
    }

    @Test
    public void testValues2() {
        final JobDetails jobDetails = new UnusableJobDetails(JobId.of("x"), JobRunnerKey.of("z"), RUN_ONCE_PER_CLUSTER,
                runOnce(new Date(42L)), null, null, CAUSE);
        assertEquals(JobId.of("x"), jobDetails.getJobId());
        assertEquals(JobRunnerKey.of("z"), jobDetails.getJobRunnerKey());
        assertEquals(RUN_ONCE_PER_CLUSTER, jobDetails.getRunMode());
        assertEquals(runOnce(new Date(42L)), jobDetails.getSchedule());
        assertNull(jobDetails.getNextRunTime());
        assertNull(((AbstractJobDetails) jobDetails).getRawParameters());
        assertFalse("Should not be runnable", jobDetails.isRunnable());
    }
}
