package com.atlassian.scheduler.core.impl;

import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.core.RunningJob;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Date;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * @since v1.0
 */
@ThreadSafe
public final class RunningJobImpl implements RunningJob {
    private final long startTime;
    private final JobId jobId;
    private final JobConfig jobConfig;

    private volatile boolean cancelled;

    public RunningJobImpl(final Date startTime, final JobId jobId,
                          final JobConfig jobConfig) {
        this.startTime = requireNonNull(startTime, "startTime").getTime();
        this.jobId = requireNonNull(jobId, "jobId");
        this.jobConfig = requireNonNull(jobConfig, "jobConfig");
    }


    @Nonnull
    @Override
    public Date getStartTime() {
        return new Date(startTime);
    }

    @Nonnull
    @Override
    public JobId getJobId() {
        return jobId;
    }

    @Nonnull
    @Override
    public JobConfig getJobConfig() {
        return jobConfig;
    }

    @Override
    public boolean isCancellationRequested() {
        return cancelled;
    }

    @Override
    public void cancel() {
        cancelled = true;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final RunningJobImpl other = (RunningJobImpl) o;
        return startTime == other.startTime
                && jobId.equals(other.jobId)
                && jobConfig.equals(other.jobConfig);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, jobId, jobConfig);
    }

    @Override
    public String toString() {
        return "RunningJobImpl[startTime=" + startTime +
                ",jobId=" + jobId +
                ",jobConfig=" + jobConfig +
                ",cancelled=" + cancelled +
                ']';
    }
}
