/**
 * Core implementation support classes.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.core.impl;

import javax.annotation.ParametersAreNonnullByDefault;