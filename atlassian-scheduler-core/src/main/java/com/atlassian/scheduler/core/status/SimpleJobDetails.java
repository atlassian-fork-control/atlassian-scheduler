package com.atlassian.scheduler.core.status;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import static com.atlassian.scheduler.util.Safe.copy;

/**
 * A simple, concrete implementation of {@link JobDetails} that has the parameter map present.
 *
 * @since v1.0
 */
public final class SimpleJobDetails extends AbstractJobDetails {
    private final Map<String, Serializable> parameters;

    public SimpleJobDetails(final JobId jobId, final JobRunnerKey jobRunnerKey, final RunMode runMode,
                            final Schedule schedule, @Nullable final Date nextRunTime, final byte[] rawParameters,
                            @Nullable final Map<String, Serializable> parameters) {
        super(jobId, jobRunnerKey, runMode, schedule, nextRunTime, rawParameters);
        this.parameters = copy(parameters);
    }

    @Override
    @Nonnull
    public Map<String, Serializable> getParameters() {
        return parameters;
    }

    @Override
    public boolean isRunnable() {
        return true;
    }

    @Override
    protected void appendToStringDetails(final StringBuilder sb) {
        sb.append(",parameters=").append(parameters);
    }
}

