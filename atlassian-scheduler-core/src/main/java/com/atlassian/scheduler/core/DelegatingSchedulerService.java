package com.atlassian.scheduler.core;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * A scheduler service that delegates all requests to another scheduler service.
 * The intended purpose is to allow applications to instantiate one of the
 * {@link AbstractSchedulerService} implementations as a {@link LifecycleAwareSchedulerService}
 * visible only to the application and register this restricted implementation as
 * the {@link SchedulerService} that is available to add-ons.
 *
 * @since v1.0
 */
@SuppressWarnings("unused")
public class DelegatingSchedulerService implements SchedulerService {
    private final SchedulerService delegate;

    public DelegatingSchedulerService(SchedulerService delegate) {
        this.delegate = requireNonNull(delegate, "delegate");
    }

    @Override
    public void registerJobRunner(JobRunnerKey jobRunnerKey, JobRunner jobRunner) {
        delegate.registerJobRunner(jobRunnerKey, jobRunner);
    }

    @Override
    public void unregisterJobRunner(JobRunnerKey jobRunnerKey) {
        delegate.unregisterJobRunner(jobRunnerKey);
    }

    @Override
    @Nonnull
    public Set<JobRunnerKey> getRegisteredJobRunnerKeys() {
        return delegate.getRegisteredJobRunnerKeys();
    }

    @Override
    @Nonnull
    public Set<JobRunnerKey> getJobRunnerKeysForAllScheduledJobs() {
        return delegate.getJobRunnerKeysForAllScheduledJobs();
    }

    @Override
    public void scheduleJob(JobId jobId, JobConfig jobConfig) throws SchedulerServiceException {
        delegate.scheduleJob(jobId, jobConfig);
    }

    @Override
    @Nonnull
    public JobId scheduleJobWithGeneratedId(JobConfig jobConfig) throws SchedulerServiceException {
        return delegate.scheduleJobWithGeneratedId(jobConfig);
    }

    @Override
    public void unscheduleJob(JobId jobId) {
        delegate.unscheduleJob(jobId);
    }

    @Override
    @Nullable
    public Date calculateNextRunTime(Schedule schedule) throws SchedulerServiceException {
        return delegate.calculateNextRunTime(schedule);
    }

    @Override
    @CheckForNull
    public JobDetails getJobDetails(JobId jobId) {
        return delegate.getJobDetails(jobId);
    }

    @Override
    @Nonnull
    public List<JobDetails> getJobsByJobRunnerKey(JobRunnerKey jobRunnerKey) {
        return delegate.getJobsByJobRunnerKey(jobRunnerKey);
    }
}
