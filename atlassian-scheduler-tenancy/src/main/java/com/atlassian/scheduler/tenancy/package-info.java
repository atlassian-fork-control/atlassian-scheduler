/**
 * Provides tenant-aware decoration for a scheduler service.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.tenancy;

import javax.annotation.ParametersAreNonnullByDefault;