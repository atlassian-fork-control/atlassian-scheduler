package com.atlassian.scheduler.config;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.Serializable;

import static java.util.Objects.requireNonNull;

/**
 * A wrapper to distinguish job IDs from simple strings and to make it easier
 * to avoid confusing them with job runner keys.
 *
 * @since v1.0
 */
@Immutable
@PublicApi
public final class JobId implements Serializable, Comparable<JobId> {
    private static final long serialVersionUID = 1L;

    /**
     * Wraps the provided string as a {@code JobId}.
     *
     * @param id the job ID, as a string
     * @return the wrapped job runner key
     */
    public static JobId of(String id) {
        return new JobId(id);
    }


    private final String id;

    private JobId(final String id) {
        this.id = requireNonNull(id, "id");
    }


    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        return o != null
                && o.getClass() == getClass()
                && ((JobId) o).id.equals(id);
    }

    @Override
    public int compareTo(final JobId o) {
        return id.compareTo(o.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String toString() {
        return id;
    }
}
