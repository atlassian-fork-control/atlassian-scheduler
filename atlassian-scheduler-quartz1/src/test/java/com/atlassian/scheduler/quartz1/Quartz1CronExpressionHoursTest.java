package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.core.tests.CronExpressionHoursTest;

/**
 * @since v1.5
 */
public class Quartz1CronExpressionHoursTest extends CronExpressionHoursTest {
    public Quartz1CronExpressionHoursTest() {
        super(new Quartz1CronFactory());
    }
}
