package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.core.tests.CronExpressionSecondsTest;

/**
 * @since v1.5
 */
public class Quartz1CronExpressionSecondsTest extends CronExpressionSecondsTest {
    public Quartz1CronExpressionSecondsTest() {
        super(new Quartz1CronFactory());
    }
}

