package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.core.tests.CronExpressionTimingTest;
import org.junit.Ignore;

/**
 * @since v1.5
 */
@Ignore("Slow; run manually if interested")
public class Quartz1CronExpressionTimingTest extends CronExpressionTimingTest {
    public Quartz1CronExpressionTimingTest() {
        super(new Quartz1CronFactory());
    }

    public static void main(String[] args) {
        new Quartz1CronExpressionTimingTest().test();
    }
}
