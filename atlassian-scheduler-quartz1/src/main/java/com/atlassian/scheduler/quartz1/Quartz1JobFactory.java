package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.core.AbstractSchedulerService;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.simpl.SimpleJobFactory;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

import static java.util.Objects.requireNonNull;

/**
 * Assigned to the real scheduler so that we can escape the Quartz-centric world.
 *
 * @since v1.0
 */
class Quartz1JobFactory extends SimpleJobFactory implements JobFactory {
    private final AbstractSchedulerService schedulerService;
    private final RunMode schedulerRunMode;

    Quartz1JobFactory(AbstractSchedulerService schedulerService, RunMode schedulerRunMode) {
        this.schedulerService = requireNonNull(schedulerService, "schedulerService");
        this.schedulerRunMode = requireNonNull(schedulerRunMode, "schedulerRunMode");
    }

    @Override
    public Job newJob(final TriggerFiredBundle bundle) throws SchedulerException {
        final JobDetail jobDetail = bundle.getJobDetail();
        if (Quartz1Job.class.equals(jobDetail.getJobClass())) {
            return new Quartz1Job(schedulerService, schedulerRunMode, bundle);
        }
        return new ClassLoaderProtectingWrappedJob(super.newJob(bundle), schedulerService);
    }

    // SCHEDULER-11: Ensure that the Job runs with its own class loader set as the thread's CCL
    static class ClassLoaderProtectingWrappedJob implements Job {
        private final Job delegate;
        private final AbstractSchedulerService service;

        ClassLoaderProtectingWrappedJob(Job delegate, AbstractSchedulerService service) {
            this.service = service;
            this.delegate = requireNonNull(delegate, "delegate");
        }

        @Override
        public void execute(final JobExecutionContext context) throws JobExecutionException {
            service.preJob();
            final Thread thd = Thread.currentThread();
            final ClassLoader originalClassLoader = thd.getContextClassLoader();
            try {
                thd.setContextClassLoader(delegate.getClass().getClassLoader());
                delegate.execute(context);
            } finally {
                thd.setContextClassLoader(originalClassLoader);
                service.postJob();
            }
        }
    }
}
