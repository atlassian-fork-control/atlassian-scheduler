/**
 * Implements that {@code atlassian-scheduler} API using Quartz 1.x.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.quartz1;

import javax.annotation.ParametersAreNonnullByDefault;