package com.atlassian.scheduler.core.tests;

import org.junit.Test;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.satisfiedBy;
import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.unsatisfiedBy;

/**
 * Tests covering the functionality of the seconds cron field.
 *
 * @since v1.5
 */
public abstract class CronExpressionSecondsTest extends AbstractCronExpressionTest {
    protected CronExpressionSecondsTest(CronFactory cronFactory) {
        super(cronFactory);
    }

    private void secondsBy17s(final String secondsExpr) {
        assertCron(secondsExpr, secondsExpr + " 0 0 1 1 ?",
                satisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(1, 0, 0, 1, 1, 2014),
                unsatisfiedBy(16, 0, 0, 1, 1, 2014),
                satisfiedBy(17, 0, 0, 1, 1, 2014),
                unsatisfiedBy(20, 0, 0, 1, 1, 2014),
                satisfiedBy(34, 0, 0, 1, 1, 2014),
                satisfiedBy(51, 0, 0, 1, 1, 2014),
                unsatisfiedBy(52, 0, 0, 1, 1, 2014),
                unsatisfiedBy(59, 0, 0, 1, 1, 2014));
    }

    @Test
    public void testSecondsIncrement_simple() {
        secondsBy17s("/17");
        secondsBy17s("*/17");
        secondsBy17s("0/17");
        secondsBy17s("0-52/17");

        assertCron("3/17", "3/17 0 0 1 1 ?",
                satisfiedBy(3, 0, 0, 1, 1, 2014),
                unsatisfiedBy(17, 0, 0, 1, 1, 2014),
                satisfiedBy(20, 0, 0, 1, 1, 2014),
                satisfiedBy(37, 0, 0, 1, 1, 2014),
                unsatisfiedBy(51, 0, 0, 1, 1, 2014),
                satisfiedBy(54, 0, 0, 1, 1, 2014));
    }

    @Test
    public void testSecondsIncrement_complex() {
        assertCron("1,3-5,8-30/10,42", "1,3-5,8-30/10,42 0 0 1 1 ?",
                unsatisfiedBy(0, 0, 0, 1, 1, 2014),
                satisfiedBy(1, 0, 0, 1, 1, 2014),
                unsatisfiedBy(2, 0, 0, 1, 1, 2014),
                satisfiedBy(3, 0, 0, 1, 1, 2014),
                satisfiedBy(4, 0, 0, 1, 1, 2014),
                satisfiedBy(5, 0, 0, 1, 1, 2014),
                unsatisfiedBy(6, 0, 0, 1, 1, 2014),
                unsatisfiedBy(7, 0, 0, 1, 1, 2014),
                satisfiedBy(8, 0, 0, 1, 1, 2014),
                unsatisfiedBy(9, 0, 0, 1, 1, 2014),
                unsatisfiedBy(17, 0, 0, 1, 1, 2014),
                satisfiedBy(18, 0, 0, 1, 1, 2014),
                unsatisfiedBy(19, 0, 0, 1, 1, 2014),
                satisfiedBy(28, 0, 0, 1, 1, 2014),
                unsatisfiedBy(30, 0, 0, 1, 1, 2014),
                unsatisfiedBy(38, 0, 0, 1, 1, 2014),
                satisfiedBy(42, 0, 0, 1, 1, 2014));
    }

    @Test
    public void testSecondsIncrement_normalRange() {
        assertCron("0-50/17", "0-50/17 0 0 1 1 ?",
                satisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(7, 0, 0, 1, 1, 2014),
                satisfiedBy(17, 0, 0, 1, 1, 2014),
                unsatisfiedBy(24, 0, 0, 1, 1, 2014),
                satisfiedBy(34, 0, 0, 1, 1, 2014),
                unsatisfiedBy(50, 0, 0, 1, 1, 2014),
                unsatisfiedBy(51, 0, 0, 1, 1, 2014));

        assertCron("20-50/10", "20-50/10 0 0 1 1 ?",
                unsatisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(10, 0, 0, 1, 1, 2014),
                satisfiedBy(20, 0, 0, 1, 1, 2014),
                satisfiedBy(30, 0, 0, 1, 1, 2014),
                satisfiedBy(40, 0, 0, 1, 1, 2014),
                satisfiedBy(50, 0, 0, 1, 1, 2014));

        assertCron("3-37/17", "3-37/17 0 0 1 1 ?",
                satisfiedBy(3, 0, 0, 1, 1, 2014),
                unsatisfiedBy(17, 0, 0, 1, 1, 2014),
                satisfiedBy(20, 0, 0, 1, 1, 2014),
                unsatisfiedBy(21, 0, 0, 1, 1, 2014),
                satisfiedBy(37, 0, 0, 1, 1, 2014),
                unsatisfiedBy(54, 0, 0, 1, 1, 2014),
                unsatisfiedBy(59, 0, 0, 1, 1, 2014));
    }

    @Test
    public void testSecondsIncrement_wrappedRange() {
        assertCron("50-0/17", "50-0/17 0 0 1 1 ?",
                unsatisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(7, 0, 0, 1, 1, 2014),
                unsatisfiedBy(17, 0, 0, 1, 1, 2014),
                unsatisfiedBy(24, 0, 0, 1, 1, 2014),
                unsatisfiedBy(34, 0, 0, 1, 1, 2014),
                satisfiedBy(50, 0, 0, 1, 1, 2014),
                unsatisfiedBy(51, 0, 0, 1, 1, 2014));

        assertCron("50-20/10", "50-20/10 0 0 1 1 ?",
                satisfiedBy(0, 0, 0, 1, 1, 2014),
                satisfiedBy(10, 0, 0, 1, 1, 2014),
                satisfiedBy(20, 0, 0, 1, 1, 2014),
                unsatisfiedBy(30, 0, 0, 1, 1, 2014),
                unsatisfiedBy(40, 0, 0, 1, 1, 2014),
                satisfiedBy(50, 0, 0, 1, 1, 2014));

        assertCron("37-3/17", "37-3/17 0 0 1 1 ?",
                unsatisfiedBy(3, 0, 0, 1, 1, 2014),
                unsatisfiedBy(11, 0, 0, 1, 1, 2014),
                unsatisfiedBy(17, 0, 0, 1, 1, 2014),
                unsatisfiedBy(20, 0, 0, 1, 1, 2014),
                unsatisfiedBy(21, 0, 0, 1, 1, 2014),
                satisfiedBy(37, 0, 0, 1, 1, 2014),
                satisfiedBy(54, 0, 0, 1, 1, 2014),
                unsatisfiedBy(59, 0, 0, 1, 1, 2014));
    }
}
