package com.atlassian.scheduler.core.tests;

import com.atlassian.scheduler.cron.CronExpressionValidator;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Objects;

import static com.atlassian.scheduler.cron.ErrorCode.COMMA_WITH_LAST_DOM;
import static com.atlassian.scheduler.cron.ErrorCode.COMMA_WITH_WEEKDAY_DOM;
import static com.atlassian.scheduler.cron.ErrorCode.ILLEGAL_CHARACTER;
import static com.atlassian.scheduler.cron.ErrorCode.ILLEGAL_CHARACTER_AFTER_INTERVAL;
import static com.atlassian.scheduler.cron.ErrorCode.ILLEGAL_CHARACTER_AFTER_QM;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME_DAY_OF_WEEK;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME_FIELD;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME_RANGE;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_DAY_OF_MONTH;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_DAY_OF_WEEK;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_HOUR;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_MONTH;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_SEC_OR_MIN;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_YEAR;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_YEAR_RANGE;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_DAY_OF_MONTH;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_DAY_OF_WEEK;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_HOUR;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_MONTH;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_SECOND_OR_MINUTE;
import static com.atlassian.scheduler.cron.ErrorCode.QM_CANNOT_USE_FOR_BOTH_DAYS;
import static com.atlassian.scheduler.cron.ErrorCode.QM_CANNOT_USE_HERE;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_END_OF_EXPRESSION;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_TOKEN_FLAG_L;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_TOKEN_FLAG_W;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_TOKEN_HASH;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Core compatibility test for a CronExpressionValidator.
 *
 * @since v1.4
 */
public abstract class CronExpressionValidatorTest {
    protected CronExpressionValidator validator;

    @Test
    public void testEmptyString() {
        assertError("", UNEXPECTED_END_OF_EXPRESSION, "Unexpected end of expression", null, 0);
    }

    @Test
    public void testMissingFields() {
        assertError("0 0 0 ? 1", UNEXPECTED_END_OF_EXPRESSION, "Unexpected end of expression", null, 9);
    }

    @Test
    public void testTruncatedExpression() {
        assertError("0 0 0 ? 1 NO", INVALID_NAME_DAY_OF_WEEK, "Invalid day-of-week name: 'NO'", "NO", 10);
    }

    @Test
    public void testNameRanges() {
        assertInvalidNameRange("0 0 0 ? 1 1-FRI", 12);
        assertInvalidNameRange("0 0 0 ? 1 MON-4", 14);
        assertInvalidNameRange("0 0 0 ? 1 MON-45", 14);
        assertInvalidNameRange("0 0 0 ? 1 MON-456", 14);
        assertInvalidNameRange("0 0 0 ? 1 MON-45F", 14);
        assertInvalidNameDayOfWeek("0 0 0 ? 1 MON-FFF", "FFF", 14);
        assertValid("0 0 0 ? 1 MON-FRI");
    }

    @Test
    public void testNameRangesWrongLength() {
        assertInvalidNameDayOfWeek("0 0 0 ? 1 M-FRI", "M", 10);
        assertInvalidNameDayOfWeek("0 0 0 ? 1 MO-FRI", "MO", 10);
        assertInvalidNameDayOfWeek("0 0 0 ? 1 MON-F", "F", 14);
        assertInvalidNameDayOfWeek("0 0 0 ? 1 MON-FR", "FR", 14);
        assertInvalidName("0 0 0 WL 1 ?", "WL", 6);
    }

    @Test
    public void testCronDoesNotGrokThatCharacter() {
        assertInvalidNameField("0 XXX 0 ? 1 MON-TUE", "XXX", 2);
        assertErrorQuartzExempt("0 0XY 0 ? 1 MON-TUE", ILLEGAL_CHARACTER, "Unexpected character: 'X'", "X", 3,
                "the trailing 'XY' just gets ignored");
        assertErrorQuartzExempt("0 0-3XY 0 ? 1 MON-TUE", ILLEGAL_CHARACTER, "Unexpected character: 'X'", "X", 5,
                "the trailing 'XY' just gets ignored");
    }

    @Test
    public void testQuestionMarkWithNonWhitespaceAfterIt() {
        assertValid("0 0 0 ? 1 MON-TUE");
        assertValid("0 0 0 ? 1 Mon-tUe");
        assertValid("0 0 0 ?\t1 mon-tue");
        assertErrorQuartzExempt("0 0 0 ?X 1 MON-TUE", ILLEGAL_CHARACTER_AFTER_QM, "Illegal character after '?': 'X'",
                "X", 7,
                "it has an off-by-one error in its parser and misses the X.");
        assertError("0 0 0 ?XY 1 MON-TUE", ILLEGAL_CHARACTER_AFTER_QM, "Illegal character after '?': 'X'", "X", 7);
    }

    @Test
    public void testQuestionMarkInNonDayField() {
        assertError("0 ? 0 ? 1 1", QM_CANNOT_USE_HERE,
                "You can only use '?' for the day-of-month or the day-of-week.", null, 2);
    }

    @Test
    public void testQuestionMarkInBothDayFields() {
        assertError("0 0 0 ? 1 ?", QM_CANNOT_USE_FOR_BOTH_DAYS,
                "You cannot specify '?' for both the day-of-month and the day-of-week.", null, 10);
    }

    @Test
    public void testMisplacedCommas() {
        assertError("0 0 0 L,3 1 ?", COMMA_WITH_LAST_DOM,
                "You cannot use 'L' or 'W' with multiple day-of-month values.", null, 7);
        assertError("0 0 0 3,LW 1 ?", COMMA_WITH_LAST_DOM,
                "You cannot use 'L' or 'W' with multiple day-of-month values.", null, 8);
        assertError("0 0 0 L-4W,7 1 ?", COMMA_WITH_LAST_DOM,
                "You cannot use 'L' or 'W' with multiple day-of-month values.", null, 10);
        assertErrorQuartzExempt("0 0 0 11W,17,22-23 1 ?", COMMA_WITH_WEEKDAY_DOM,
                "You cannot use 'W' with multiple day-of-month values.", null, 9,
                "it interprets this as '11W'");
        assertErrorQuartzExempt("0 0 0 13,17W,22-23 1 ?", COMMA_WITH_WEEKDAY_DOM,
                "You cannot use 'W' with multiple day-of-month values.", null, 11,
                "it interprets this as '13W'");
        assertErrorQuartzExempt("0 0 0 13,17-19W,23 1 ?", COMMA_WITH_WEEKDAY_DOM,
                "You cannot use 'W' with multiple day-of-month values.", null, 14,
                "it ignores the 'W' flag entirely");
    }

    @Test
    public void testSupportForLastMinusNumberForDayOfMonth() {
        assertValid("0 0 0 L-3 1 ?");
        assertValid("0 0 0 L-4W 1 ?");
    }

    @Test
    public void testBadDayOfMonthExpressions() {
        assertInvalidFlagL("0 0 0 L- 1 ?", 6);
        assertInvalidFlagL("0 0 0 L-X 1 ?", 6);
        assertInvalidFlagL("0 0 0 L-XW 1 ?", 6);
        assertError("0 0 0 W-2 1 ?", UNEXPECTED_TOKEN_FLAG_W, "The 'W' option was used incorrectly.", null, 6);
        assertInvalidNameField("0 0 0 XYZ 1 ?", "XYZ", 6);
    }

    @Test
    public void testLastFlagWithHyphenW() {
        assertValid("0 0 0 L-W 1 ?");
    }

    @Test
    public void testLastFlagSpecifiedForUnsupportedField() {
        assertErrorQuartzExempt("L 0 0 1 1 ?", UNEXPECTED_TOKEN_FLAG_L, "The 'L' option was used incorrectly.", null, 0,
                "the invalid L is ignored, there are no values for seconds, and this never matches.");
        assertErrorQuartzExempt("0 L 0 1 1 ?", UNEXPECTED_TOKEN_FLAG_L, "The 'L' option was used incorrectly.", null, 2,
                "the invalid L is ignored, there are no values for minutes, and this never matches.");
        assertInvalidFlagL("6L 0 0 1 1 ?", 1);
    }

    @Test
    public void testWeekdayFlagSpecifiedForUnsupportedField() {
        assertValid("0 0 0 LW 1 ?");
        assertValid("0 0 0 15W 1 ?");
        assertError("6W 0 0 1 1 ?", UNEXPECTED_TOKEN_FLAG_W, "The 'W' option was used incorrectly.", null, 1);
    }

    @Test
    public void testHashOption() {
        assertValid("0 0 0 ? 1 1#3");
        assertValid("0 0 0 ? 1 MON#3");
        assertError("6#4 0 0 1 1 ?", UNEXPECTED_TOKEN_HASH, "The '#' option was used incorrectly.", null, 1);
    }

    @Test
    public void testTrailingGarbageIgnoredIfYearsArePresent() {
        assertValid("0 0 0 * 1 ? 2000-2099 Then a comment with spe\u00A9ial chars and *,L+& all of that.");
        assertError("0 0 0 * 1 ? But not unless the year is given", INVALID_NAME_FIELD,
                "This field does not support names: 'BUT'", "BUT", 12);
    }

    @Test
    public void testNumericValueOutOfRange() {
        assertError("65 0 0 ? 1 MON", INVALID_NUMBER_SEC_OR_MIN,
                "The values for seconds and minutes must be from 0 to 59.", null, 0);
        assertError("0 65 0 ? 1 MON", INVALID_NUMBER_SEC_OR_MIN,
                "The values for seconds and minutes must be from 0 to 59.", null, 2);
        assertError("0 0 26 ? 1 MON", INVALID_NUMBER_HOUR,
                "The values for hours must be from 0 to 23.", null, 4);
        assertError("0 0 0 33 1 ?", INVALID_NUMBER_DAY_OF_MONTH,
                "The values for day-of-month must be from 1 to 31.", null, 6);
        assertError("0 0 0 ? 15 MON", INVALID_NUMBER_MONTH,
                "The values for month must be from 1 to 12.", null, 8);
        assertError("0 0 0 ? 1 9", INVALID_NUMBER_DAY_OF_WEEK,
                "The values for day-of-week must be from 1 to 7.", null, 10);
        assertErrorQuartzExempt("0 0 0 ? 1 6 42", INVALID_NUMBER_YEAR,
                "The values for year must be from 1970 to 2299.", null, 12,
                "it does not check that the years are reasonable during the parse.");
        assertErrorQuartzExempt("0 0 0 ? 1 6 9999", INVALID_NUMBER_YEAR,
                "The values for year must be from 1970 to 2299.", null, 12,
                "it does not check that the years are reasonable during the parse.");
        assertError("0 0 0 ? 1 6 2036-2016", INVALID_NUMBER_YEAR_RANGE,
                "Year ranges must specify the earlier year first.", null, 17);
    }

    @Test
    public void testInvalidIncrements() {
        assertError("0 / 0 ? 1 MON", INVALID_STEP,
                "The step interval character '/' must be followed by a positive integer.", null, 3);
        assertError("/65 0 0 ? 1 MON", INVALID_STEP_SECOND_OR_MINUTE,
                "The step interval for second or minute must be less than 60: 65", "65", 1);
        assertError("0 /65 0 ? 1 MON", INVALID_STEP_SECOND_OR_MINUTE,
                "The step interval for second or minute must be less than 60: 65", "65", 3);
        assertError("0 0 /26 ? 1 MON", INVALID_STEP_HOUR,
                "The step interval for hour must be less than 24: 26", "26", 5);
        assertError("0 0 0 /36 1 ?", INVALID_STEP_DAY_OF_MONTH,
                "The step interval for day-of-month must be less than 31: 36", "36", 7);
        assertError("0 0 0 ? /15 MON", INVALID_STEP_MONTH,
                "The step interval for month must be less than 12: 15", "15", 9);
        assertError("0 0 0 ? 1 /9", INVALID_STEP_DAY_OF_WEEK,
                "The step interval for day-of-week must be less than 7: 9", "9", 11);
        assertError("0 0 0 1/A 1 ?", INVALID_STEP,
                "The step interval character '/' must be followed by a positive integer.", null, 8);
        assertError("0 0 0 1/6A 1 ?", ILLEGAL_CHARACTER_AFTER_INTERVAL,
                "Illegal character after '/': 'A'", "A", 9);
    }

    // Actual cron expressions encountered in the Atlassian Cloud that Quartz accepts but Caesium rejected
    @Test
    public void testRealWorldErrorsFromCloud() {
        // Caesium bug that is now fixed
        assertValid(" \t 0 0 0 ? 1 MON-TUE");

        // Quartz bugs
        assertErrorQuartzExempt("0 0/120 8-16 ? * MON-FRI", INVALID_STEP_SECOND_OR_MINUTE,
                "The step interval for second or minute must be less than 60: 120", "120", 4,
                "it only validates the step interval if the range has an upper bound.");
        assertErrorQuartzExempt("0 0/2 8-18 ? * MON-FRI *?", ILLEGAL_CHARACTER,
                "Unexpected character: '?'", "?", 24,
                "it silently ignores anything after the '*' that isn't a step interval.");
        assertErrorQuartzExempt("0 40 9 1W,15W 1-12 ? 2010-2050", COMMA_WITH_WEEKDAY_DOM,
                "You cannot use 'W' with multiple day-of-month values.", null, 9,
                "it interprets this as '11W'");
    }


    protected void assertValid(final String cronExpression) {
        try {
            validator.validate(cronExpression);
        } catch (CronSyntaxException cse) {
            final Error e = new AssertionError("Expected '" + cronExpression + "' to be valid, but it threw " + cse);
            e.initCause(cse);
            throw e;
        }
    }

    protected void assertError(final String cronExpression, final ErrorCode errorCode, final String message,
                               final String value, final int errorOffset) {
        try {
            validator.validate(cronExpression);
            fail("Expected a CronSyntaxException with message=\"" + message + "\" and errorOffset=" + errorOffset);
        } catch (CronSyntaxException cse) {
            assertThat(cse, exception(cronExpression.toUpperCase(Locale.US), errorCode, message, value, errorOffset));
        }
    }

    /**
     * Factored out into its own method because the Quartz implementations frequently return {@code -1} instead
     * of the correct error offset.
     *
     * @param expected the expected error offset
     * @param cse      the actual exception
     */
    protected boolean verifyErrorOffset(final int expected, final CronSyntaxException cse) {
        return expected == cse.getErrorOffset();
    }

    /**
     * Assert that the given cron expression is invalid, but that the Quartz implementations will incorrectly
     * say it is valid.
     * <p>
     * It's really sad that this is necessary, but there are just so many places where Quartz is just plain
     * wrong that doing this any other way would be even more confusing.
     * </p>
     *
     * @param cronExpression   the invalid cron expression
     * @param errorCode        the expected error code
     * @param message          the expected error message
     * @param value            the expected token value
     * @param pos              the expected error offset
     * @param whyQuartzIsWrong an explanation as to what Quartz does instead and why it is wrong
     */
    protected void assertErrorQuartzExempt(final String cronExpression, final ErrorCode errorCode, final String message,
                                           final String value, final int pos, final String whyQuartzIsWrong) {
        assertError(cronExpression, errorCode, message, value, pos);
    }

    protected void assertInvalidFlagL(final String cronExpression, final int errorOffset) {
        assertError(cronExpression, UNEXPECTED_TOKEN_FLAG_L,
                "The 'L' option was used incorrectly.",
                null,
                errorOffset);
    }

    protected void assertInvalidName(final String cronExpression, final String name, final int errorOffset) {
        assertError(cronExpression, INVALID_NAME,
                "Invalid name: '" + name + '\'',
                name,
                errorOffset);
    }

    protected void assertInvalidNameField(final String cronExpression, final String name, final int errorOffset) {
        assertError(cronExpression, INVALID_NAME_FIELD,
                "This field does not support names: '" + name + '\'',
                name,
                errorOffset);
    }

    protected void assertInvalidNameRange(final String cronExpression, final int errorOffset) {
        assertError(cronExpression, INVALID_NAME_RANGE,
                "Cannot specify a range using month or day-of-week names unless a valid name is used for both bounds.",
                null,
                errorOffset);
    }

    protected void assertInvalidNameDayOfWeek(final String cronExpression, final String name, final int errorOffset) {
        assertError(cronExpression, INVALID_NAME_DAY_OF_WEEK,
                "Invalid day-of-week name: '" + name + '\'',
                name,
                errorOffset);
    }

    protected Matcher<CronSyntaxException> exception(final String cronExpression, final ErrorCode errorCode,
                                                     final String message, final String value, final int errorOffset) {
        return new CronSyntaxExceptionMatcher(cronExpression, errorCode, message, value, errorOffset);
    }

    protected static String stackTrace(Throwable e) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, false);
        e.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }


    class CronSyntaxExceptionMatcher extends TypeSafeMatcher<CronSyntaxException> {
        private final String cronExpression;
        private final ErrorCode errorCode;
        private final String message;
        private final String value;
        private final int errorOffset;

        CronSyntaxExceptionMatcher(String cronExpression, ErrorCode errorCode, String message, String value, int errorOffset) {
            this.cronExpression = cronExpression;
            this.errorCode = errorCode;
            this.message = message;
            this.value = value;
            this.errorOffset = errorOffset;
        }

        @Override
        protected boolean matchesSafely(CronSyntaxException cse) {
            return Objects.equals(cronExpression, cse.getCronExpression())
                    && errorCode == cse.getErrorCode()
                    && Objects.equals(message, cse.getMessage())
                    && Objects.equals(value, cse.getValue())
                    && verifyErrorOffset(errorOffset, cse);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("a CronSyntaxException with:\n\tcronExpression: ").appendText(cronExpression)
                    .appendText("\n\t\terrorCode: ").appendText(errorCode.name())
                    .appendText("\n\t\tmessage: ").appendText(message)
                    .appendText("\n\t\tvalue: ").appendText(value)
                    .appendText("\n\t\terrorOffset: ").appendText(String.valueOf(errorOffset));
        }

        @Override
        protected void describeMismatchSafely(CronSyntaxException cse, Description description) {
            description.appendText("a CronSyntaxException with:\n\tcronExpression: ").appendText(cse.getCronExpression())
                    .appendText("\n\t\terrorCode: ").appendText(cse.getErrorCode().name())
                    .appendText("\n\t\tmessage: ").appendText(cse.getMessage())
                    .appendText("\n\t\tvalue: ").appendText(cse.getValue())
                    .appendText("\n\t\terrorOffset: ").appendText(String.valueOf(cse.getErrorOffset()))
                    .appendText("\n").appendText(stackTrace(cse))
                    .appendText("\n");
        }
    }
}

