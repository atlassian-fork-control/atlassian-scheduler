package com.atlassian.scheduler.core.tests;

import com.atlassian.scheduler.core.tests.CronFactory.CronExpressionAdapter;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import java.util.Date;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

/**
 * Cron expression to date matcher.
 * <p>
 * Note that the order of the arguments matches that of the cron expressions so that it is
 * easier to tell by inspection whether or not the expression should match.
 * </p>
 *
 * @since v1.5
 */
public class CronExpressionAssertions {
    public static Matcher<CronExpressionAdapter> satisfiedBy(
            final int second, final int minute, final int hour,
            final int day, final int month, final int year) {
        return satisfiedBy(second, minute, hour, day, month, year, DateTimeZone.getDefault());
    }

    public static Matcher<CronExpressionAdapter> satisfiedBy(
            final int second, final int minute, final int hour,
            final int day, final int month, final int year,
            final DateTimeZone zone) {
        return new TypeSafeMatcher<CronExpressionAdapter>() {
            @SuppressWarnings("MagicConstant")
            @Override
            protected boolean matchesSafely(final CronExpressionAdapter cronExpression) {
                final LocalDateTime dateTime = new LocalDateTime(year, month, day, hour, minute, second);
                return cronExpression.isSatisfiedBy(dateTime.toDateTime(zone).toDate());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("(")
                        .appendText(s(second)).appendText(", ")
                        .appendText(s(minute)).appendText(", ")
                        .appendText(s(hour)).appendText(", ")
                        .appendText(s(day)).appendText(", ")
                        .appendText(s(month)).appendText(", ")
                        .appendText(s(year)).appendText(", ")
                        .appendText(zone.getID()).appendText(")");
            }
        };
    }

    public static Matcher<CronExpressionAdapter> unsatisfiedBy(
            int second, int minute, int hour,
            int day, int month, int year) {
        return not(satisfiedBy(second, minute, hour, day, month, year));
    }

    public static Matcher<CronExpressionAdapter> unsatisfiedBy(
            int second, int minute, int hour,
            int day, int month, int year,
            DateTimeZone zone) {
        return not(satisfiedBy(second, minute, hour, day, month, year, zone));
    }

    public static Matcher<? super Date> runTime(final long timestamp) {
        return new TypeSafeMatcher<Date>() {
            @Override
            protected boolean matchesSafely(Date item) {
                return item.getTime() == timestamp;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("<Date with timestamp=")
                        .appendText(String.valueOf(timestamp))
                        .appendText("=")
                        .appendText(new Date(timestamp).toString())
                        .appendText(">");
            }

            @Override
            protected void describeMismatchSafely(Date item, Description mismatchDescription) {
                mismatchDescription.appendText("<Date with timestamp=")
                        .appendText(String.valueOf(item.getTime()))
                        .appendText("=")
                        .appendText(item.toString())
                        .appendText(">");
            }
        };
    }

    public static Matcher<Date> runTime(
            final int second, final int minute, final int hour,
            final int day, final int month, final int year,
            final DateTimeZone zone) {
        return new TypeSafeMatcher<Date>() {
            @Override
            protected boolean matchesSafely(final Date result) {
                final DateTime dateTime = new DateTime(result, zone);
                return dateTime.getYear() == year
                        && dateTime.getMonthOfYear() == month
                        && dateTime.getDayOfMonth() == day
                        && dateTime.getHourOfDay() == hour
                        && dateTime.getMinuteOfHour() == minute
                        && dateTime.getSecondOfMinute() == second
                        && dateTime.getMillisOfSecond() == 0;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("(")
                        .appendText(s(second)).appendText(", ")
                        .appendText(s(minute)).appendText(", ")
                        .appendText(s(hour)).appendText(", ")
                        .appendText(s(day)).appendText(", ")
                        .appendText(s(month)).appendText(", ")
                        .appendText(s(year)).appendText(", ")
                        .appendText(zone.getID()).appendText(")");
            }

            @Override
            protected void describeMismatchSafely(Date item, Description mismatchDescription) {
                final DateTime dateTime = new DateTime(item, zone);
                mismatchDescription.appendText("(")
                        .appendText(s(dateTime.getSecondOfMinute())).appendText(", ")
                        .appendText(s(dateTime.getMinuteOfHour())).appendText(", ")
                        .appendText(s(dateTime.getHourOfDay())).appendText(", ")
                        .appendText(s(dateTime.getDayOfMonth())).appendText(", ")
                        .appendText(s(dateTime.getMonthOfYear())).appendText(", ")
                        .appendText(s(dateTime.getYear())).appendText(", ")
                        .appendText(zone.getID()).appendText(")");
            }
        };
    }

    public static Matcher<Date> runTime(
            final int second, final int minute, final int hour,
            final int day, final int month, final int year,
            final DateTimeZone zone, final long millis) {
        //noinspection unchecked
        return allOf(runTime(millis), runTime(second, minute, hour, day, month, year, zone));
    }

    static String s(int value) {
        return String.valueOf(value);
    }
}
