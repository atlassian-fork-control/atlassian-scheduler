package com.atlassian.scheduler.core.tests;

import org.junit.Test;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.satisfiedBy;
import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.unsatisfiedBy;

/**
 * @since v1.5
 */
public abstract class CronExpressionMonthTest extends AbstractCronExpressionTest {
    public CronExpressionMonthTest(CronFactory cronFactory) {
        super(cronFactory);
    }

    @Test
    public void testMonthName_single() {
        assertCron("Checking FEB", "0 0 0 ? FEB 2",
                satisfiedBy(0, 0, 0, 3, 2, 2014),
                satisfiedBy(0, 0, 0, 10, 2, 2014),
                satisfiedBy(0, 0, 0, 17, 2, 2014),
                satisfiedBy(0, 0, 0, 24, 2, 2014),
                unsatisfiedBy(1, 0, 0, 3, 2, 2014),
                unsatisfiedBy(0, 1, 0, 3, 2, 2014),
                unsatisfiedBy(0, 0, 1, 3, 2, 2014),
                unsatisfiedBy(0, 0, 0, 4, 2, 2014),
                unsatisfiedBy(0, 0, 0, 3, 3, 2014),
                satisfiedBy(0, 0, 0, 2, 2, 2015));
    }

    @Test
    public void testMonthName_normalRange() {
        assertCron("Checking FEB-NOV - Other field mismatches", "1 1 1 ? FEB-NOV 2",
                unsatisfiedBy(0, 1, 1, 3, 2, 2014),
                unsatisfiedBy(2, 1, 1, 3, 2, 2014),
                unsatisfiedBy(1, 0, 1, 3, 2, 2014),
                unsatisfiedBy(1, 2, 1, 3, 2, 2014),
                unsatisfiedBy(1, 1, 0, 3, 2, 2014),
                unsatisfiedBy(1, 1, 2, 3, 2, 2014));

        assertCron("Checking FEB-NOV - February", "1 1 1 ? FEB-NOV MON",
                satisfiedBy(1, 1, 1, 3, 2, 2014),
                satisfiedBy(1, 1, 1, 10, 2, 2014),
                satisfiedBy(1, 1, 1, 17, 2, 2014),
                satisfiedBy(1, 1, 1, 24, 2, 2014),
                unsatisfiedBy(1, 1, 1, 2, 2, 2014),
                unsatisfiedBy(1, 1, 1, 4, 2, 2014),
                satisfiedBy(1, 1, 1, 2, 2, 2015));

        assertCron("Checking FEB-NOV - Other months", "1 1 1 ? FEB-NOV 2",
                unsatisfiedBy(1, 1, 1, 6, 1, 2014),  // January
                unsatisfiedBy(1, 1, 1, 2, 3, 2014),  // March
                satisfiedBy(1, 1, 1, 3, 3, 2014),
                unsatisfiedBy(1, 1, 1, 4, 3, 2014),
                unsatisfiedBy(1, 1, 1, 6, 4, 2014),  // April
                satisfiedBy(1, 1, 1, 7, 4, 2014),
                unsatisfiedBy(1, 1, 1, 8, 4, 2014),
                unsatisfiedBy(1, 1, 1, 4, 7, 2014),  // July
                unsatisfiedBy(1, 1, 1, 23, 11, 2014),  // November
                satisfiedBy(1, 1, 1, 24, 11, 2014),
                unsatisfiedBy(1, 1, 1, 25, 11, 2014),
                unsatisfiedBy(1, 1, 1, 7, 12, 2014),  // December
                unsatisfiedBy(1, 1, 1, 8, 12, 2014),
                unsatisfiedBy(1, 1, 1, 9, 12, 2014));
    }

    @Test
    public void testCronMonthName_wrappingRange() {
        assertCron("Checking NOV-FEB", "1 1 1 ? NOV-FEB MON",
                satisfiedBy(1, 1, 1, 6, 1, 2014),  // January
                unsatisfiedBy(1, 1, 1, 2, 2, 2014),  // February
                satisfiedBy(1, 1, 1, 3, 2, 2014),
                unsatisfiedBy(1, 1, 1, 4, 2, 2014),
                unsatisfiedBy(1, 1, 1, 6, 4, 2014),  // April
                unsatisfiedBy(1, 1, 1, 7, 4, 2014),
                unsatisfiedBy(1, 1, 1, 8, 4, 2014),
                unsatisfiedBy(1, 1, 1, 4, 7, 2014),  // July
                unsatisfiedBy(1, 1, 1, 23, 11, 2014),  // November
                satisfiedBy(1, 1, 1, 24, 11, 2014),
                unsatisfiedBy(1, 1, 1, 25, 11, 2014),
                unsatisfiedBy(1, 1, 1, 7, 12, 2014),  // December
                satisfiedBy(1, 1, 1, 8, 12, 2014),
                unsatisfiedBy(1, 1, 1, 9, 12, 2014));
    }
}
